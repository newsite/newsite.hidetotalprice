var NSCrmHideTotalPrice = BX.namespace('NSCrmHideTotalPrice');

NSCrmHideTotalPrice.init = function () {
	console.log('NSCrmHideTotalPrice init');

	BX.addCustomEvent('kanban.grid:onrender', function (params) {
		NSCrmHideTotalPrice.initPriceToggle()
	});
	BX.addCustomEvent('bx.main.filter:customentityblur', function (params) {
		NSCrmHideTotalPrice.insertMenuItem()
	});
};

// скрытие общей суммы и прослушивание чекбокса
NSCrmHideTotalPrice.initPriceToggle = function () {
	var prices = $('.crm-kanban-total-price')

	prices.toggle(!!JSON.parse(localStorage.getItem('show-total-price-kanban')))

	$(document.body).off('change.NSCrmHideTotalPrice').on('change.NSCrmHideTotalPrice', '.js-hide-total-price-toggle', function () {
		var inputStatus = $(this).prop('checked')
		localStorage.setItem('show-total-price-kanban', JSON.stringify(inputStatus))
		prices.toggle(inputStatus)
	})
}

// вставка чекбокса в выпадающее меню
NSCrmHideTotalPrice.insertMenuItem = function () {
	var parent = $('#popup-window-content-toolbar_deal_list_settings_menu')

	if (parent.length) {
		var menu = parent.find('.menu-popup-items')
		var checkbox = $(`
			<label class="menu-popup-item menu-popup-no-icon  checkbox-custom">
				<input class="checkbox-custom__input  js-hide-total-price-toggle" type="checkbox" ${!!JSON.parse(localStorage.getItem('show-total-price-kanban')) ? 'checked' : ''}>
				<i class="checkbox-custom__icon"></i>
				<span class="menu-popup-item-text  checkbox-custom__text">Показать общую сумму сделок</span>
			</label>
		`)
		if (!menu.find('.checkbox-custom').length) {
			menu.append(checkbox)
		}
	}
}
