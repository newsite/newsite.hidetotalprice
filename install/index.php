<?

includeModuleLangFile(__FILE__);

$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen("/install/index.php"));

if (!defined("HIDE_TOTAL_PRICE_MODULE_DIR_PATH")) {
	require_once $strPath2Lang . "/include.php";
}

if (class_exists(str_replace(".", "_", HIDE_TOTAL_PRICE_MODULE_NAME))) {
	return;
}

Class newsite_hidetotalprice extends CModule
{

	var $MODULE_ID = 'newsite.hidetotalprice';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME = 'Скрытие общей суммы сделок в канбан CRM';
	var $MODULE_DESCRIPTION = 'Переключает видимость общей суммы';
	var $MODULE_GROUP_RIGHTS = "Y";


    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->PARTNER_NAME = 'newsite';
        $this->PARTNER_URI = 'https://newsite.by/';
    }

	function DoInstall()
	{
		global $APPLICATION;

		$this->InstallFiles();
		$this->InstallDB();
		$GLOBALS["errors"] = $this->errors;

        LocalRedirect($APPLICATION->GetCurPage());
	}

	function DoUninstall()
	{
		global $APPLICATION;

        $this->UnInstallDB();
        $this->UnInstallFiles();
        $GLOBALS["errors"] = $this->errors;

        LocalRedirect($APPLICATION->GetCurPage());
	}

	function InstallDB()
	{
		global $DB, $APPLICATION;

		RegisterModuleDependences("main", "OnProlog", HIDE_TOTAL_PRICE_MODULE_NAME, "\\Newsite\\HideTotalPrice\\Events", "onProlog");
		RegisterModule(HIDE_TOTAL_PRICE_MODULE_NAME);

		return true;
	}

	function UnInstallDB($arParams = array())
	{
		CModule::IncludeModule($this->MODULE_ID);

		global $DB, $APPLICATION;

		$this->errors = false;

		UnRegisterModuleDependences("main", "OnProlog", HIDE_TOTAL_PRICE_MODULE_NAME, "\\Newsite\\HideTotalPrice\\Events", "onProlog");
		UnRegisterModule(HIDE_TOTAL_PRICE_MODULE_NAME);

        return true;
	}

	function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . HIDE_TOTAL_PRICE_MODULE_DIR_PATH . "install/files", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/newsite.hide_total_price/", true, true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . HIDE_TOTAL_PRICE_MODULE_DIR_PATH . "install/files", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/newsite.hide_total_price/");
		return true;
	}
}
