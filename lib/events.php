<?

	namespace Newsite\HideTotalPrice;

	class Events
	{
		public static function onProlog() {

			\CJSCore::RegisterExt('NSCrmHideTotalPrice',
				array(
					'js' => [
						'/bitrix/js/newsite.hide_total_price/NSCrmHideTotalPrice.js',
					],
					'css' => [
						'/bitrix/js/newsite.hide_total_price/NSCrmHideTotalPrice.css',
					],
					'rel' => array(
						'jquery',
					),
				)
			);

			\CJSCore::Init('NSCrmHideTotalPrice');

			$asset = \Bitrix\Main\Page\Asset::getInstance();
			$asset->addString('<script>BX.ready(function () {BX.NSCrmHideTotalPrice.init();});</script>');
		}
	}
